<?php

/*
 * Generates form for registration
 *
 * @return form as array
 */
function tupas_registration_registration_form($form, &$form_state, $user_data) {
  $form = array();
  $form_state['user_data'] = $user_data;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['password'] = array(
    '#type' => 'password_confirm',
    '#size' => 32,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register')
  );
  return $form;
}
